# bash completion for reflector-antergos        -*- shell-script -*-

_mylistselector()
{
    local cur="$1"
    local list_elems_all="$2"

    local prefix realcur elems word

    realcur="${cur##*,}"
    prefix="${cur%$realcur}"

    for word in $list_elems_all
    do
        if ! [[ $prefix == *"$word"* ]]; then
            elems="$word $elems"
        fi
    done
    COMPREPLY=( $(compgen -P "$prefix" -W "$elems" -S ',' -- $realcur) )
    compopt -o nospace
}

_reflector_antergos() 
{
    local cur prev #words cword split
    _init_completion -s || return

    local mirrors
    local protocols="https http"
    local countrygroups="America Asia Europe Asia2 Europe2"
    local countries="Automated Bulgaria Canada China Czech_Republic Denmark France"
          countries+=" England Germany Greece Japan Netherlands Portugal Russia Spain"
	  countries+=" Sweden USA"
    if [ -f /etc/pacman.d/antergos-mirrorlist.pacnew ] ; then
	mirrors=$(grep "^Server" /etc/pacman.d/antergos-mirrorlist.pacnew | sed 's|^Server = ||')
    fi

    # Note that currently reflector-antergos does not support multi-word country names
    # with option --exclude-countries. In such names, spaces must be converted to underscores.
	
    case "$prev" in
        --quiet|--verbose|-h|--help|--save)
            return
            ;;
        --exclude-countrygroups)
            _mylistselector "$cur" "$countrygroups"
            return
            ;;
        --exclude-countries)
            _mylistselector "$cur" "$countries"
            return
            ;;
        --include-countries)
            _mylistselector "$cur" "$countries"
            return
            ;;
        --exclude-mirrors)
            _mylistselector "$cur" "$mirrors"
            return
            ;;
        --protocols)
            _mylistselector "$cur" "$protocols"
            return
            ;;
        --hide-original-mirrors)
            COMPREPLY=( $(compgen -W "yes no" -- "${cur}") )
            return
            ;;
        --timeout)
            COMPREPLY+=( $(compgen -P "$cur" -W "{0..9}") )
            compopt -o nospace
            return
            ;;
        --repo)
            COMPREPLY=( $(compgen -W "antergos" -- "${cur}") )
            return
            ;;
        --arch)
            COMPREPLY=( $(compgen -W "x86_64" -- "${cur}") )
            return
            ;;
    esac

    local opts="--exclude-countrygroups= --exclude-countries= --exclude-mirrors="
    opts+=" --include-countries="
    opts+=" --hide-original-mirrors= --protocols= --timeout="
    opts+=" --save --quiet --verbose --help -h"
    opts+=" --repo= --arch="                                              # deprecated

    case "$cur" in
        -*|"")
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
            compopt -o nospace
            return
            ;;
    esac
} &&
complete -F _reflector_antergos reflector-antergos
